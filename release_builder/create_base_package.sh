#!/bin/bash

#####################################################################################


if [ "$WORKSPACE" == "" ]; then
    echo "ERROR: Missing WORKSPACE environment variable."
    exit 1
fi

if [ ! -d "$WORKSPACE/fgdata" ]; then
    echo "No fgdata subdir in WORKSPACE: can't continue"
    exit 1
fi


VERSION=`cat fgdata/version`
BASE_VERSION_TAG="version/2024.1.1"
SCENERY_PACK_AIRPORT=BIKF
SCENERY_PACK_URI="https://sourceforge.net/projects/flightgear/files/scenery/SceneryPack.${SCENERY_PACK_AIRPORT}.tgz/download"

echo "Assembling base package for $VERSION"
cd $WORKSPACE


# wipe directories and re-create
rm -rf staging
mkdir -p output
mkdir -p staging

# wipe existing data TXZs
rm -f output/FlightGear-$VERSION*data.txz

rsync -az --exclude-from=release_builder/base_package_excludes fgdata staging/

# add all the scenery pack files into it

SCENERY_PACK_NAME=SceneryPack_${SCENERY_PACK_AIRPORT}.tgz

# Should we re-download the SceneryPack periodically? Or just rely on doing a workspace wipe?
if [ ! -f $SCENERY_PACK_NAME ]; then
    echo "Download scenery pack from ${SCENERY_PACK_URI}"
    # -L to follow the SF redirect
    curl -L $SCENERY_PACK_URI --output $SCENERY_PACK_NAME
fi

tar -xf $SCENERY_PACK_NAME --directory staging/fgdata
pushd staging/fgdata
mv SceneryPack.${SCENERY_PACK_AIRPORT} Scenery
popd

# create SHA sums file
#find staging -type f -exec shasum -a 256 {} > allsums.txt \;

echo "Computing dir-indexes"
./release_builder/create_dirindex.sh staging/fgdata


# Creating full base package TXZ 
# import we re-apply excludes here, so that the root .dirindex is skipped

OUTPUT_NAME=FlightGear-$VERSION-data
tar -cJf output/$OUTPUT_NAME.txz --exclude-from=release_builder/base_package_excludes --directory staging fgdata

# echo "Done, data TXZs are in output/"

# sample scp command to upload to SourceForge:
# assuming you have a suitable alias 'sourceforge-frs' in your .ssh/config
# eg:
# Host sourceforge-frs
#   User jmturner
#   HostName frs.sourceforge.net

# scp output/FlightGear-2024.1.1-data.txz sourceforge-frs:/home/frs/project/flightgear/release-2024.1
