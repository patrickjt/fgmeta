# Download & Compile

# CI builds on GitLab

## CMake deployment helpers

The file `flightgear/CMakeModules/Installation.cmake` contains various helpers to minimsie shell scripting
after the build completes. In general, CMake scripting is preferred because the build system has complete
information available to it about library locations, versions, etc.

Notably there are CMake targets or optional installation components for:

- adding the Qt deployables on Windows, by running `windeployqt`
- creating a suitable `appdir` for running a Linux AppImage creation, including generating the `.desktop` meta-data file
- copying OSG libraires and plugin into the deployment locations, eg the `appdir` for Linux and the bundle on macOS
- creating the InnoSetup config file (`InstallConfig.iss`) on Windows
- uploading debug symbols to Senty.io

## macOS code signing, packaging, etc

We rely on a `.keychain` file being added via the GitLab 'secure files' feature. The keychain contains the macOS developer ID
certificate and corresponding private key file. The keychain also contains a notarization profile with saved AppStoreConnect data.

The developer ID, notarization profile name and the keychain password are all configured in
the GitLab UI via masked variables. The relevant variables are:

- `FG_CODESIGN_IDENTITY`: this is the certifcate identity passed to `codesign`
- `FG_NOTARISE_PROFILE`: the notarisation profile name inside the keytchain
- `FG_KEYCHAIN_PASSWORD`: password to unlock the keychain

The CI YAML contains shell commands to download the secure file, add the keychain to the system list,
and unlock it so that code-signing and notarization are possible. Unlocking the keychain ensures
that operations which use it don't block with a password dialog or prompt.

## Windows code signing

# 