REM SPDX-FileCopyrightText: James Turner <james@flightgear.org>
REM
REM SPDX-License-Identifier: GPL-2.0

IF NOT DEFINED WORKSPACE SET WORKSPACE=%~dp0

REM build setup
ECHO Packaging root is %WORKSPACE%

REM not neeed inside the VM
REM subst X: /D -
subst X: %WORKSPACE%.

REM ensure output dir is clean since we upload the entirety of it
rmdir /S /Q output

ECHO #define IncludeData "FALSE" >> InstallConfig.iss

iscc /Q FlightGear.iss
