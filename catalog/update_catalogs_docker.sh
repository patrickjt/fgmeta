#!/bin/sh

script_home=
work_path=/fgaddon-work

if [ ! -d "$work_path" ]; then
    echo "No work path mounted: can't continue"
    exit 1
fi

# add the scripts to the path
catalog_dir=$script_home/fgmeta/catalog
output_dir=$work_path/output

# update fgmeta
git -C "$script_home/fgmeta" pull

export PATH=$PATH:$catalog_dir
export PYTHONPATH=$script_home/fgmeta/python3-flightgear

cd "$work_path"

echo "Generating trunk catalog"

update-catalog.py --quiet --update "$catalog_dir/fgaddon-catalog-ukmirror"

echo "Generating stable catalog 2024"
update-catalog.py --quiet --update "$catalog_dir/stable-2024-catalog"

echo "Generating stable catalog 2020"

update-catalog.py --quiet --update "$catalog_dir/stable-2020-catalog"



echo "All done successfully"
exit 0
