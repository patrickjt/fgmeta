#!/usr/bin/ruby

require 'ERB'
require 'time'
require 'fileutils' #I know, no underscore is not ruby-like
include FileUtils

require 'optparse'

options = {:dist => Dir.pwd + "dist", :osg => Dir.pwd + "dist"}

OptionParser.new do |opt|
  opt.on('--dist <distdir>') { |o| options[:dist] = o }
  opt.on('--osg <osgdist>') { |o| options[:osg] = o }
end.parse!

$prefixDir=options[:dist]
$osgDir=options[:osg]

# from http://drawingablank.me/blog/ruby-boolean-typecasting.html
class String
  def to_bool
    return true if self == true || self =~ (/^(true|t|yes|y|1)$/i)
    return false if self == false || self.empty? || self =~ (/^(false|f|no|n|0)$/i)
    raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
  end
end

class NilClass
  def to_bool; false; end
end

$codeSignIdentity = ENV['FG_CODESIGN_IDENTITY']
$keychain = ENV['FG_KEYCHAIN']
$keychainProfile=ENV['FG_NOTARIZE_PROFILE']

#puts "Code signing identity is #{$codeSignIdentity}"
#puts "Keychain is at #{$keychain}"

$isRelease = ENV['FG_IS_RELEASE'].to_bool
puts "Is-release? : ##{$isRelease}"

dmgDir=Dir.pwd + "/image"
srcDir=Dir.pwd + "/flightgear"
qmlDir=srcDir + "/src/GUI/qml"
puts "Erasing previous image dir"
`rm -rf #{dmgDir}`

bundle=dmgDir + "/FlightGear.app"

# remove some Qt plugins we don't want (easier than trying to filter beforehand)

pluginsDir="#{$prefixDir}/FlightGear.app/Contents/PlugIns"
`rm -rf #{pluginsDir}/printsupport #{pluginsDir}/sqldrivers #{pluginsDir}/iconengines`

puts "Copying app bundle"
`mkdir -p #{dmgDir}`
`rsync  --archive --quiet #{$prefixDir}/FlightGear.app/ #{bundle}`

contents=bundle + "/Contents"
macosDir=contents + "/MacOS"
$frameworksDir=contents +"/Frameworks"
resourcesDir=contents+"/Resources"


fgVersion = File.read("#{srcDir}/flightgear-version").strip

if $isRelease
  dmgPath = Dir.pwd + "/output/FlightGear-#{fgVersion}.dmg"
  volName="\"FlightGear #{fgVersion}\""
else
  # nightly build
  n = Date.today
  dateStr = n.strftime('%Y%m%d')
  dmgPath = Dir.pwd + "/output/FlightGear-#{dateStr}-nightly.dmg"
  volName="\"FlightGear Nightly #{dateStr}\""
end

# hidutil create won't create enclosing directories
`mkdir -p output`

# FIXME once the CMake-based copying is working
otherLibs = ['dbus-1.3']
otherLibs.each do |l|
  `cp #{$prefixDir}/lib/lib#{l}.dylib #{$frameworksDir}`
end

if File.exist?("#{$prefixDir}/bin/fgcom-data")
  puts "Copying FGCom data files"
  `ditto #{$prefixDir}/bin/fgcom-data #{resourcesDir}/fgcom-data`
end

`cp #{srcDir}/COPYING #{dmgDir}`

# move documentation to a public place
#`cp fgdata/Docs/FGShortRef.pdf "#{dmgDir}/Quick Reference.pdf"`
#`cp fgdata/Docs/getstart.pdf "#{dmgDir}/Getting Started.pdf"`

createArgs = "-format ULMO -volname #{volName}"

# enable the hardened runtime and timestamp options, so notarization works
codeSignArgs = "-v --deep --options=runtime --timestamp"

# code sign the entire bundle once complete - v2 code-signing
puts "Signing #{bundle}"
`codesign #{codeSignArgs} --keychain #{$keychain} -s "#{$codeSignIdentity}" #{bundle}`
puts "Creating DMG"

`rm -f #{dmgPath}`
`hdiutil create -srcfolder #{dmgDir} #{createArgs} #{dmgPath}`

puts "Notarizing DMG #{dmgPath}"
`xcrun notarytool submit #{dmgPath} --wait --keychain #{$keychain} --keychain-profile #{$keychainProfile} `
