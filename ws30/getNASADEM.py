#!/usr/bin/python3

from zipfile import ZipFile
import os
import subprocess
import sys

PROGNAME = os.path.basename(sys.argv[0])
DEFAULT_HGT_DIR = os.path.join("/home", "flightgear", "data")
DEFAULT_OUTPUT_DIR = os.path.join("/home", "flightgear", "output", "vpb")
DEFAULT_SERVER = "https://e4ftl01.cr.usgs.gov/MEASURES/NASADEM_HGT.001/2000.02.11/"
DEBUG = False

def usage(msg, *, exit_status=1, **kwargs):
    help_text = f"""\
ERROR: {msg}

Get HGT DEM files from a NASADEM server (defaults to {DEFAULT_SERVER}), for use by genVPB.py or osgdem.

To create a username, see https://urs.earthdata.nasa.gov/users/new/

Usage: {PROGNAME} <min-lat> <min-lon> <max-lat> <max-lon> <username> <password>[--server <server>] [--hgt-dir <hgt-dir>]

  <min-lat> <min-lon> <max-lat> <max-lon - bounding box of scenery to generated
  <username>      - NASA Earthdata Login username
  <password>      - NASA Earthdata Login password
  [server]        - Optional server to retrieve from.  Defaults to {DEFAULT_SERVER}
  [hgt-dir]       - Optional directory containing unzipped NASADEM HGT files. Defaults to '{DEFAULT_HGT_DIR}'"""
    print(help_text, **kwargs)
    sys.exit(exit_status)

def getHGTFilename(lon: int, lat: int) -> str :
    ns = "n" if lat >= 0 else "s"
    ew = "e" if lon >= 0 else "w"
    hgt_filename = "{ns}{lat:02d}{ew}{lon:03d}.hgt".format(
        ew=ew, ns=ns, lat=abs(lat), lon=abs(lon))
    return hgt_filename

def getZipFilename(lon: int, lat: int) -> str :
    ns = "n" if lat >= 0 else "s"
    ew = "e" if lon >= 0 else "w"
    hgt_filename = "NASADEM_HGT_{ns}{lat:02d}{ew}{lon:03d}.zip".format(
        ew=ew, ns=ns, lat=abs(lat), lon=abs(lon))
    return hgt_filename

def hgtExists(lon: int, lat: int) -> bool :
    ns = "n" if lat >= 0 else "s"
    ew = "e" if lon >= 0 else "w"
    hgt_filename = getHGTFilename(lon, lat)
    hgtname = os.path.join(hgtdir, hgt_filename)

    if os.path.isfile(hgtname):
        print(f"HGT file for {lon}, {lat} already exists:{hgtname}.  Skipping")
        return True
    return False

def getHGT(lon: int, lat: int, hgtdir: str, username: str, password: str, server: str) -> bool :
    hgt_filename = getHGTFilename(lon, lat)
    zip_filename = getZipFilename(lon, lat)
    zip_path = os.path.join(hgtdir, zip_filename)
    hgt_path = os.path.join(hgtdir, hgt_filename)

    if os.path.isfile(hgt_path) : return True

    # Sadly the python requests library doesn't appear
    # to be quite robust enough to handle the redirect/authentication
    # chain here so we have to use wget
    args = [
        "wget",
        f"--user={username}",
        f"--password={password}",
        f"--output-document={zip_path}",
        f"{server}{zip_filename}"
    ]

    if DEBUG : print(args)

    try :
        cp = subprocess.run(args, capture_output=True, check=True, text=True)
        if DEBUG : print("stdout: '{}'".format(cp.stdout))
        if DEBUG : print("stderr: '{}'".format(cp.stderr))
    except subprocess.CalledProcessError as ex:
        if ex.returncode == 8:
            # This is a 404 error and perfectly normal if there is no land in this 1x1 degree tile
            if DEBUG : print(f"HGT file {server}{zip_filename} does not exist (404).")
            # wget may still generate a 0 byte file in this case, so remove it
            if os.path.isfile(zip_path) : os.unlink(zip_path)
            return False
        else :
            # Anything else is a more significant error and so should go up the stack
            raise
    else :

        # We should now have a zipfile to unzip
        if os.path.isfile(zip_path) :
            with ZipFile(zip_path) as zf :
                zf.extract(hgt_filename, hgtdir)
                if os.path.isfile(hgt_path) :
                    if DEBUG : print(f"Successfully downloaded {hgt_filename}")
                    # We've extracted all we need, so remove the zip-file
                    os.unlink(zip_path)

                    # We're done.
                    return True
                    
                else :
                    print(f"Failed to extract {hgt_filename} from {zip_filename}")
        else :
            print(f"Failed to find {zip_filename} to extract.")
    
    return False

def main():

    if len(sys.argv) < 7:
        usage("")

    lat0, lon0, lat1, lon1 = map(int, sys.argv[1:5])
    username = sys.argv[5]
    password = sys.argv[6]
    hgtdir = DEFAULT_HGT_DIR
    server = DEFAULT_SERVER

    # Parse optional command line arguments.
    i = 7
    while i < len(sys.argv):
        arg = sys.argv[i]

        if arg == '--server':
            i = i + 1
            server = str(sys.argv[i])

        elif arg == '--hgt-dir':
            i = i + 1
            hgtdir = str(sys.argv[i])

        else:
            usage("Invalid argument: {}".format(arg))

        i = i + 1

    # Basic argument validation
    if lon0 >= lon1:
        usage("<min-lon> must be smaller than <max-lon>")
    if lat0 >= lat1:
        usage("<min-lat> must be smaller than <max-lat>")
    if not os.path.isdir(hgtdir):
        usage("<hgt-dir> (" + hgtdir + ") does not exist")

    # Now do the work
    for lon in range(lon0, lon1):
        for lat in range(lat0, lat1):            
            print(f"Getting {lon}, {lat}")
            success = getHGT(lon=lon, lat=lat, hgtdir=hgtdir, username=username, password=password, server=server)
            if not success :
                print(f"Failed to get HGT tile for {lat}, {lon}")

if __name__ == "__main__": main()
