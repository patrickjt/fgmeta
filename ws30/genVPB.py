#!/usr/bin/python3

import numpy
import os
import shlex
import subprocess
import sys
from osgeo import gdal
from osgeo import ogr
import zipfile
import getNASADEM
import genwaterraster
import getSentinel2

gdal.UseExceptions()    # Enable exceptions

# Set to True to generate intermediat .tif files.  Useful to see what may have gone wrong
DEBUG = False

# Configure compression of the final data.  Configuration provided for backwards compatibility
COMPRESS = True

# The Ocean landclass value is used throughout for masking
OCEAN_LC = 44

PROGNAME = os.path.basename(sys.argv[0])
DEFAULT_HGT_DIR = os.path.join("/home", "flightgear", "data")
DEFAULT_OUTPUT_DIR = os.path.join("/home", "flightgear", "output", "vpb")

lat0 = 0
lon0 = 0
lat1 = 0
lon1 = 0
bbox_defined = False
input_raster = None
hgt_dir = DEFAULT_HGT_DIR
output_dir = DEFAULT_OUTPUT_DIR
nasadem_server = getNASADEM.DEFAULT_SERVER
nasadem_user = None
nasadem_password = None
coastline = None
reclass = None
shrink_water = 0
gen_water_raster = False
cache_dir = None
sentinel = False
raster_dir = None
download = False

def usage(msg, *, exit_status=1, **kwargs):
    help_text = f"""\
ERROR: {msg}

Generate a set of VPB tiles from a landclass raster and HGT elevation data, optionally reclassifying the raster data, applying coastline polygon data as a mask,
shrinking water areas, and generating a water raster.

Usage: {PROGNAME} --raster <input-raster> [ option ... ]
Usage: {PROGNAME} --bbox <lat0> <lon0> <lat1> <lon1> --sentinel --reclass <reclass> [ option ... ]


  Arguments:

  --raster <input-raster>   - Input landclass raster
  --bbox <lat0> <lon0> <lat1> <lon1> - Bounding box of scenery to be generated.
  --sentinel <raster-dir>   - Using Sentinel-2 Landclass data from <raster-dir>
  --auto-download           - Auto-download Sentinel-2 Landclass data.  Only valid with --sentinel
  --hgt-dir <hgt-dir>       - Set directory containing unzipped NASADEM HGT files. Defaults to '{DEFAULT_HGT_DIR}'.
  --output-dir <output-dir> - Set output directory. Defaults to '{DEFAULT_OUTPUT_DIR}'.
  --reclass <reclass>       - Reclassify raster using file <reclass>. See fgmeta/ws30/mappings/.
  --coastline <coastline>   - Clip against coastline against polygon (.osm).  E.g. from https://osmdata.openstreetmap.de/data/land-polygons.html.
  --shrink-water <pixels>   - Shrink water bodies (landclasses 40, 41) by <pixels> pixels, typically in combination with generating a water raster.
  --generate-water-raster   - Generate a water raster from OSM data.
  --cache-dir <dir>         - Use <dir> as a cache of OSM data to improve performance and reduce load on Overpass API.
  --nasadem-server <server> - Set server to download NASADEM data from. Defaults to {getNASADEM.DEFAULT_SERVER}
  --nasadem-user <user>     - NASA Earthdata username. Required to download NASADEM automatically. To create a username, see https://urs.earthdata.nasa.gov/users/new/.
  --nasadem-password <password> - NASA Earthdata password. Required to download NASADEM automatically.
  --debug                   - Debug output, generate intermediate geoTIFFs and don't compress output.
  """

    print(help_text, **kwargs)
    sys.exit(exit_status)

def error(msg, exit_status=1, **kwargs):
    print(msg, **kwargs)
    sys.exit(exit_status)

def getTileBaseName(lon: int, lat: int) -> str:
    # Determine a name for the 1x1 degree tile, to match the standard for WS3.0 for simplicity
    ns = "n" if lat >= 0 else "s"
    ew = "e" if lon >= 0 else "w"
    return "ws_{ew}{lon:03d}{ns}{lat:02d}".format(ew=ew, ns=ns, lon=abs(lon), lat=abs(lat))


def osWalkErrorHandling(exception):
    raise exception             # don't silence errors during os.walk()


def compressToZip(destfile, base_dir):
    """Recursively archive files from a directory to a .zip file.

    Positional arguments:

    destfile -- path to the destination zip file
    base_dir -- compress files recursively found there

    If already existing, destfile is truncated. Each file found under
    the directory tree starting at base_dir is stored at the root of the
    archive, as if using zip(1) with the -j option. This can of course
    lead to several archive members having the same name!

    Note that we explicitly write a new file here, so any existing archive
    that we want to update needs to be uncompressed into the output directory
    first.
    """
    with zipfile.ZipFile(destfile, 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as compressed_file:
        for dirpath, dirs, files in os.walk(base_dir,
                                            onerror=osWalkErrorHandling):
            for f in files:
                compressed_file.write(os.path.join(dirpath, f),
                                      os.path.basename(f))


def writeGeoTIFF(filename : str, data_source : gdal.Dataset) -> str:
    os.makedirs(output_dir, exist_ok=True)
    output_path = os.path.join(output_dir, filename + ".tif")
    if DEBUG : print("Writing " + output_path)

    driver = gdal.GetDriverByName("GTiff")
    outdata = driver.Create(output_path, data_source.RasterXSize, data_source.RasterYSize, 1, gdal.GDT_Byte)
    outdata.SetProjection(data_source.GetProjection())
    outdata.SetGeoTransform(data_source.GetGeoTransform())
    band = data_source.GetRasterBand(1)
    arr = band.ReadAsArray()
    outband = outdata.GetRasterBand(1)
    outband.WriteArray(arr)
    if (band.GetNoDataValue() == None) :
        outband.SetNoDataValue(0)
    else :
        outband.SetNoDataValue(band.GetNoDataValue())
    outdata.FlushCache()
    outdata = None
    band = None

    return output_path

# Clip a given raster to the require 1x1 lon/lat tile, check for projection
def clipTileToLonLat(lon: int, lat: int, dataset : gdal.Dataset) -> gdal.Dataset :

    # Reduce to a single 1x1 degree block.  Note unusual ordering of extents.
    tile = gdal.Translate('', dataset, format = 'MEM', projWin = [lon, lat+1, lon+1, lat])

    # Fill any areas without data with Ocean
    band = tile.GetRasterBand(1)
    no_data = band.GetNoDataValue()
    lc_data = band.ReadAsArray()
    temp = numpy.equal(lc_data, no_data)
    numpy.putmask(lc_data, temp, OCEAN_LC)
    band.WriteArray(lc_data)
    lc_data = None # Flush
    band = None    # Flush to the tile

    return tile

# Reclassify a raster based on a set of rules
def reclassifyRaster(rules: str, dataset : gdal.Dataset) -> gdal.Dataset :

    file1 = open(rules, 'r')
    Lines = file1.readlines()
    
    count = 0
    reclass = []
    lookup = numpy.arange(256)

    for line in Lines:
        count += 1
        line = line.strip()
        #if DEBUG : print("Line {}: {}".format(count, line))
        # Ignore any lines starting with # for comments
        if len(line) == 0 or line[0] == '#' : 
            continue

        # Ignore everything that might be after a #, as inline comments
        q = line.split('#')

        # We've now removed any comments so what we should have is something of the form
        #
        # 1 2 3 = 23
        # * = 44
        #
        # This is to assign values 1, 2 or 3 to the value 23 and anything else to 44
        
        # Split on the equals sign.  s[0] will be a set of values to map from, while s[1] is the value to map to.
        s = q[0].split('=')

        if len(s) == 0 : continue
        if (len(s) != 2) or (len(s[0].strip()) == 0) or (len(s[1].strip()) == 0) : 
            print("Each line must contain exactly one mapping assignment for reclassification in {} line {} : {}".format(rules, count, q[0]))
            continue

        try :
            rh = int(s[1].strip())
        except ValueError :
            print("Invalid RH assigment for reclassification in {} line {} : {}".format(rules, count, s[1]))
            continue

        # Split on whitespace to get the list of values to map from
        l = s[0].split()

        for i in l:
            if i.strip() == '*' :
                # We're setting a wildcard value, so re-initialize the LUT to the wildcard value
                lookup = numpy.full(256, rh)
            else :
                try :
                    lh = int(i.strip())
                except ValueError:
                    print("Invalid LH assignment for reclassification in {} line {} : {}".format(rules, count, i))
                    continue
                reclass.append((lh, rh))

    # Now fill in the LUT
    for (lh, rh) in reclass:
        lookup[lh] = rh

    # Now use numpy to map the values
    for iBand in range(1, dataset.RasterCount + 1):
        band = dataset.GetRasterBand(iBand)

        for iY in range(dataset.RasterYSize):
            src_data = band.ReadAsArray(0, iY, dataset.RasterXSize, 1)
            dst_data = numpy.take(lookup, src_data)
            band.WriteArray(dst_data, 0, iY)        

    return dataset

# Reduce the amount of a give landclass in a dataset, typically because a more detailed
# representation will be applied downstream.
#
# This is done by replacing the specific landclass values with NoData, then using the 
# gdal FillNodata function to "grow" the surrounding landclasses into the NoData regions.
# Finally it uses a mask to fill back the original landclass.
#
def shrinkLandclass(dataset : gdal.Dataset, landclass : int, pixels : int) :
    band = dataset.GetRasterBand(1)
    array_data = band.ReadAsArray()
    no_data = band.GetNoDataValue()

    # Change the landclass values to NoData
    temp = numpy.equal(array_data, landclass)
    numpy.putmask(array_data, temp, no_data)
    band.WriteArray(array_data)
    
    # Grow into the NoData area
    for i in range(pixels) :
        result = gdal.FillNodata(targetBand = band, maskBand = None, maxSearchDist=1, smoothingIterations=0)
        if (result) : 
            usage("Failed to fill No Data")

    # Fill back the NoData with the original landclss
    array_data = band.ReadAsArray()
    temp = numpy.equal(array_data, no_data)
    numpy.putmask(array_data, temp, landclass)
    band.WriteArray(array_data)
    band = None # Flush to the dataset

# Clip a given data set to a coastline from OSM.  
#
# To handle cases where the OSM coastline is further out to sea than the raster coastline, we
# first "grow" the land, as if the sea level dropped.  Then we flood it again :)
def clipTileToCoastline(lon: int, lat: int, dataset : gdal.Dataset, coastline : str) -> gdal.Dataset :
    band = dataset.GetRasterBand(1)
    clipped_data = band.ReadAsArray()
    no_data = band.GetNoDataValue()

    temp = numpy.equal(clipped_data, 38) # Remove Salines
    numpy.putmask(clipped_data, temp, no_data)

    temp = numpy.equal(clipped_data, 39) # Remove Intertidal flats
    numpy.putmask(clipped_data, temp, no_data)

    temp = numpy.equal(clipped_data, 42) # Remove Lagoons
    numpy.putmask(clipped_data, temp, no_data)

    temp = numpy.equal(clipped_data, 43) # Remove Estuary
    numpy.putmask(clipped_data, temp, no_data)

    temp = numpy.equal(clipped_data, OCEAN_LC) # Sea and Ocean
    numpy.putmask(clipped_data, temp, no_data)

    band.WriteArray(clipped_data)
    clipped_data = None # Flush to the dataset
    band = None # Flush to the dataset
    if DEBUG : writeGeoTIFF(getTileBaseName(lon, lat) + "_noSea", dataset)

    # Grow the landclass so we can clip against some vector data.  We do this one pixel at a time
    # as otherwise we get many more interpolation artefacts
    band = dataset.GetRasterBand(1)
    for i in range(9) :
        result = gdal.FillNodata(targetBand = band, maskBand = None, maxSearchDist=1, smoothingIterations=0)
        if (result) : 
            usage("Failed to fill No Data")

    band = None  # Flush to the dataset

    if DEBUG : writeGeoTIFF(getTileBaseName(lon, lat) + "_grow", dataset)

    # Clip the vector dataset
    coast_shp = ogr.Open(coastline)
    coast_layer = coast_shp.GetLayer()
    coast_ds = gdal.GetDriverByName('MEM').Create('', dataset.RasterXSize, dataset.RasterYSize, 1, gdal.GDT_Byte)
    coast_ds.SetGeoTransform(dataset.GetGeoTransform())
    coast_ds.SetProjection(dataset.GetProjection())
    coast_band = coast_ds.GetRasterBand(1)
    coast_band.SetNoDataValue(no_data)
    gdal.RasterizeLayer(coast_ds, [1], coast_layer, burn_values=[1])
    coast_band = None # Flush to the dataset
    if DEBUG : writeGeoTIFF(getTileBaseName(lon, lat) + "_coast", coast_ds)

    coast_band = coast_ds.GetRasterBand(1)
    land_data = coast_band.ReadAsArray()
    band = dataset.GetRasterBand(1)
    lc_data = band.ReadAsArray()
    temp = numpy.not_equal(land_data, 1)
    numpy.putmask(lc_data, temp, no_data)
    band.WriteArray(lc_data)
    lc_data = None # Flush to the dataset
    band = None # Flush to the dataset

    if DEBUG : writeGeoTIFF(getTileBaseName(lon, lat) + "_clipped", dataset)

    # Set the NoDataValue to Ocean which is the Sea
    band = dataset.GetRasterBand(1)
    clipped_data = band.ReadAsArray()
    temp = numpy.equal(clipped_data, no_data)
    numpy.putmask(clipped_data, temp, OCEAN_LC)
    band.WriteArray(clipped_data)
    clipped_data = None # Flush
    band = None  # Flush to the dataset
    return dataset

def getHGTFilename(lat : int, lon : int) -> str :
    ns = "n" if lat >= 0 else "s"
    ew = "e" if lon >= 0 else "w"
    return "{ns}{lat:02d}{ew}{lon:03d}.hgt".format(ew=ew, ns=ns, lat=abs(lat), lon=abs(lon))

def checkHGT(lat : int, lon : int) -> bool :
    hgt_filename = getHGTFilename(lat, lon)
    hgtname = os.path.join(hgt_dir, hgt_filename)

    if not os.path.isfile(hgtname) and not os.path.isfile(hgtname.upper()):
        # Unable to fine file.  Download if possible.
        if nasadem_user != None :
            getNASADEM.getHGT(lon=lon, lat=lat, 
                              hgtdir=hgt_dir, 
                              username=nasadem_user, 
                              password=nasadem_password, 
                              server=nasadem_server)

    if not os.path.isfile(hgtname) and not os.path.isfile(hgtname.upper()):
            # Still no file
            print(f"Skipping   {lat}, {lon} as HGT file {hgtname} does not exist")
            return False
    return True

# Build a 1x1 VPB tile for a given lon/lon and geotiff
def buildTile(lon: int, lat: int, geotiff: str) -> None:
    ns = "n" if lat >= 0 else "s"
    ew = "e" if lon >= 0 else "w"

    lat10 = 10 * (lat // 10)
    lon10 = 10 * (lon // 10)

    d1 = "{ew}{lon10:03d}{ns}{lat10:02d}".format(
        ew=ew, ns=ns, lon10=abs(lon10), lat10=abs(lat10))
    d2 = "{ew}{lon:03}{ns}{lat:02}".format(
        ew=ew, ns=ns, lon=abs(lon), lat=abs(lat))
    dirname = os.path.join(d1, d2)
    os.makedirs(os.path.join(output_dir, dirname), exist_ok=True)

    tiledir = "ws_{ew}{lon:03d}{ns}{lat:02d}_root_L0_X0_Y0".format(ew=ew, ns=ns, lat=abs(lat), lon=abs(lon))

    tilename = "ws_{ew}{lon:03d}{ns}{lat:02d}.osgb".format(
        ew=ew, ns=ns, lat=abs(lat), lon=abs(lon))
    if DEBUG : print("Generating " + os.path.join(output_dir, dirname, tilename))

    hgt_filename = getHGTFilename(lat, lon)
    hgtname = os.path.join(hgt_dir, hgt_filename)

    if not os.path.isfile(hgtname): hgtname = hgtname.upper()

    if not os.path.isfile(hgtname):
        print(f"Skipping   {lat}, {lon} as HGT file {hgtname} does not exist")
        return

    if COMPRESS:
        # Firstly extract any existing compressed archive so we can
        # update the contents, overwriting any existing files.  In particular
        # this allows genVPB.py and genwaterraster.py to be run in any order
        zipfile_path = os.path.join(output_dir, d1, d2 + ".zip")
        base_dir = os.path.join(output_dir, dirname)

        if os.path.exists(zipfile_path):
            with zipfile.ZipFile(zipfile_path, 'r') as compressed_file:
                compressed_file.extractall(base_dir)

            # Remove any .osgb files.  This is because we compress without storing
            # path information, so the output from a previous genVPB.py run will
            # be in the base_dir, while the output of the new run will end up in
            # base_dir/tiledir, so we end up with duplicate filenames.
            for dirpath, dirs, files in os.walk(base_dir,
                                                onerror=osWalkErrorHandling):
                for f in files:
                    if f.endswith(".osgb") : os.unlink(os.path.join(dirpath, f))

    args = [
        "osgdem",
        "--TERRAIN",
        "--image-ext", "png",
        "--RGBA",
        "--no-interpolate-imagery",
        "--disable-error-diffusion",
        "--geocentric",
        "--no-mip-mapping",
        "-t", geotiff,
        "-d", hgtname,
        "-b", str(lon), str(lat), str(lon + 1), str(lat + 1),
        "--PagedLOD",
        "-l", "7",
        "--radius-to-max-visible-distance-ratio", "3",
        "-o", os.path.join(output_dir, dirname, tilename),
    ]

    cp = subprocess.run(args, capture_output=True, check=True, text=True)

    if DEBUG:
        log_file = os.path.join(output_dir, "osgdem.log")
        with open(log_file, "a", encoding="utf-8") as f:
            f.write(' '.join(map(shlex.quote, args)) + "\n")
            f.write("stdout:\n{}\n".format(cp.stdout))
            f.write("stderr:\n{}\n".format(cp.stderr))

    if COMPRESS:
        # Compress final output by creating a zip file. Note that the directory
        # structure is removed so as to work around a bug in OSG's relative
        # path loading of zip files.
        zipfile_path = os.path.join(output_dir, d1, d2 + ".zip")
        base_dir = os.path.join(output_dir, dirname)
        compressToZip(zipfile_path, base_dir)

        # Now remove the output directory carefully.  There should only be
        # files within the tile directory and base directory.
        for dirpath, dirs, files in os.walk(os.path.join(base_dir, tiledir),
                                            onerror=osWalkErrorHandling):
            for f in files:
                os.unlink(os.path.join(dirpath, f))

        os.rmdir(os.path.join(base_dir, tiledir))

        for dirpath, dirs, files in os.walk(base_dir,
                                            onerror=osWalkErrorHandling):
            for f in files:
                os.unlink(os.path.join(dirpath, f))

        os.rmdir(os.path.join(base_dir))

def main():
    global coastline, hgt_dir, output_dir, reclass, input_raster
    global lat0, lon0, lat1, lon1, bbox_defined
    global nasadem_user, nasadem_password, nasadem_server
    global DEBUG, shrink_water, gen_water_raster, cache_dir
    global sentinel, raster_dir, download

    current_raster = None
    

    if len(sys.argv) < 3:
        usage("")

    # Parse command line arguments.
    i = 1
    while i < len(sys.argv):
        arg = sys.argv[i]

        if arg == '--raster':
            i = i + 1
            input_raster = str(sys.argv[i])

        elif arg == '--bbox':
            i = i + 1
            lat0, lon0, lat1, lon1 = map(int, sys.argv[i:i+4])
            bbox_defined = True
            # Shift out all but one argument, which will be shifted out at the 
            # end of the loop
            i = i + 3

        elif arg == '--sentinel':
            i = i + 1
            raster_dir = str(sys.argv[i])
            sentinel = True

        elif arg == '--auto-download':
            download = True

        elif arg == '--coastline':
            i = i + 1
            coastline = str(sys.argv[i])

        elif arg == '--hgt-dir':
            i = i + 1
            hgt_dir = str(sys.argv[i])

        elif arg == '--output-dir':
            i = i + 1
            output_dir = str(sys.argv[i])

        elif arg == '--cache-dir':
            i = i + 1
            cache_dir = str(sys.argv[i])

        elif arg == '--reclass':
            i = i + 1
            reclass = str(sys.argv[i])

        elif arg == '--nasadem-server':
            i = i + 1
            nasadem_server = str(sys.argv[i])

        elif arg == '--nasadem-user':
            i = i + 1
            nasadem_user = str(sys.argv[i])

        elif arg == '--nasadem-password':
            i = i + 1
            nasadem_password = str(sys.argv[i])

        elif arg == '--debug':
            DEBUG = True

        elif arg == '--shrink-water':
            i = i + 1
            shrink_water = int(sys.argv[i])

        elif arg == '--generate-water-raster':
            gen_water_raster = True

        else:
            usage("Invalid argument: {}".format(arg))

        i = i + 1


    # Basic argument validation.

    if bbox_defined :
        if lon0 >= lon1:
            usage("<min-lon> must be smaller than <max-lon>")
        if lat0 >= lat1:
            usage("<min-lat> must be smaller than <max-lat>")

    # Either the user defines an existing input-raster,
    # or they include a bounding box AND request Sentinel-2
    # imagery, optionally with auto-download

    if input_raster :
        if not os.path.isfile(input_raster):
            usage("<input_raster> (" + input_raster + ") does not exist")
        if sentinel : 
            usage("--raster is mutually exclusive with --sentinel")
    else :
        if (not sentinel or not bbox_defined) : 
            usage("--raster is mandatory unless --sentinel and --bbox set")
        
    if not sentinel and download :
        usage("--download only valid when used with --sentinel")

    if sentinel and not reclass :
        usage("--reclass is mandatory with --sentinel, as Sentinel-2 data must be reclassified")
    
    if shrink_water < 0:
        usage("--shrink-water argument must be greater than 0")
    if coastline and not os.path.isfile(coastline):
        usage("<coastline> (" + coastline + ") does not exist")
    if reclass and not os.path.isfile(reclass):
        usage("<reclass> (" + reclass + ") does not exist")
    if not os.path.isdir(hgt_dir):
        usage("<hgt-dir> (" + hgt_dir + ") does not exist")
    if not os.path.isdir(output_dir):
        usage("<output-dir> (" + output_dir + ") does not exist")    
    if (not(output_dir.endswith("/vpb")) and not(output_dir.endswith("/vpb/"))):
        usage("<output-dir> incorrect - should end with /vpb")
    if cache_dir and not os.path.isdir(cache_dir):
        usage("<cache-dir> (" + cache_dir + ") does not exist")    


    if input_raster :
        # Read and verify raster

        print(f"Reading raster {input_raster} ... ", end="", flush=True)
        dataset = gdal.Open(input_raster)
        print("DONE.", flush=True)

        # Check and correct the projection
        projection = dataset.GetProjection()
        if not projection.startswith("GEOGCS[\"WGS 84\"") :
            # Correct to WGS84.  We are intentionally changing the base dataset here so we only
            # have to do this once.
            print("Warping raster to WGS84 ... ", end="", flush=True)
            dataset = gdal.Warp('', dataset, format = 'MEM', dstSRS = 'EPSG:4326')
            print("done.", flush=True)

        width = dataset.RasterXSize
        height = dataset.RasterYSize
        gt = dataset.GetGeoTransform()

        if bbox_defined :
            minx = round(gt[0],1)
            miny = round(gt[3] + width*gt[4] + height*gt[5], 1)
            maxx = round(gt[0] + width*gt[1] + height*gt[2], 1)
            maxy = round(gt[3], 1)

            if (minx > lon0 or miny > lat0 or maxx < lon1 or maxy < lat1) :
                error(f"Requested scenery range does not lie within raster bounds ({miny},{minx}) - ({maxy},{maxx})")

            print(f"Generating scenery for bounding box: ({lat0}, {lon0}) - ({lat1}, {lon1})")

        else :
            lon0 = round(gt[0])
            lat0 = round(gt[3] + width*gt[4] + height*gt[5])
            lon1 = round(gt[0] + width*gt[1] + height*gt[2])
            lat1 = round(gt[3])

            print(f"Generating scenery from raster bounds: ({lat0}, {lon0}) - ({lat1}, {lon1})")

    else :
        # If the input raster is not defined, then we know we are generating scenery using 
        # a bounding box and auto-calculating the raster filename for Sentinel-2
        print(f"Generating Sentinel-2 scenery for bounding box: ({lat0}, {lon0}) - ({lat1}, {lon1})")

    for lat in range(lat0, lat1):            
        for lon in range(lon0, lon1):
            if (checkHGT(lat=lat, lon=lon) == False) :
                continue

            print(f"Processing {lat}, {lon} : ", end="", flush=True)

            if input_raster == None :
                # If no input raster was defined we need to determine the correct
                # Sentinel-2 raster file.  We have to do this in the inner loop as the
                # defined bbox may cover multiple Sentinel-2 raster tiles
                if current_raster != os.path.join(raster_dir, getSentinel2.getTileFilename(lat, lon)) :
                    if download :
                        current_raster = getSentinel2.getSentinel(lat=lat, lon=lon, output_dir=raster_dir)
                    else :
                        current_raster = os.path.join(raster_dir, getSentinel2.getTileFilename(lat, lon))
                                        
                    if not os.path.isfile(current_raster):
                        print(f"Unable to find raster {current_raster}")
                        sys.exit(1)
                
                    print(f"Reading raster {current_raster} ... ", end="", flush=True)
                    dataset = gdal.Open(current_raster)
                    print("DONE.", flush=True)

                    # Check and correct the projection
                    projection = dataset.GetProjection()
                    if not projection.startswith("GEOGCS[\"WGS 84\"") :
                        # Correct to WGS84.  We are intentionally changing the base dataset here so we only
                        # have to do this once.
                        print("Warping raster to WGS84 ... ", end="", flush=True)
                        dataset = gdal.Warp('', dataset, format = 'MEM', dstSRS = 'EPSG:4326')
                        print("done.", flush=True)

            # Create a 1x1 tile
            print("clipping raster... ", end="", flush=True)
            tile = clipTileToLonLat(lon, lat, dataset)

            # Reclassify if required
            if reclass : 
                print("reclassifying... ", end="", flush=True)
                tile = reclassifyRaster(reclass, tile)

            if shrink_water > 0 :
                # Reduce the amount of water, as this will be replaced with a water raster later
                print("shrinking water... ", end="", flush=True)
                shrinkLandclass(tile, 40, shrink_water)
                shrinkLandclass(tile, 41, shrink_water)

            # Apply coastline if available
            if coastline : 
                print("clipping to coastline... ", end="", flush=True)
                tile = clipTileToCoastline(lon, lat, tile, coastline)

            # Write out the final geotiff
            print("writing final raster... ", end="", flush=True)
            geotiff = writeGeoTIFF(getTileBaseName(lon, lat), tile)

            # Build the VPB tile.
            print("building tile... ", end="", flush=True)
            buildTile(lon, lat, geotiff)

            # Generate the water raster if requested
            if gen_water_raster : 
                print("generating water raster ...", end="", flush=True)
                genwaterraster.genWaterRaster(lat=lat, lon=lon, scenery_dir=output_dir, cache_dir=cache_dir)

            # Remove the geotiff
            if not DEBUG : os.unlink(geotiff)
            print("DONE.", flush=True)

if __name__ == "__main__": main()
