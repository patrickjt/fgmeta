import mgrs
import os
import shutil
import os
import sys
from datetime import datetime
from pathlib import Path
from urllib.parse import urlparse
import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
from osgeo import gdal

PROGNAME = os.path.basename(sys.argv[0])
DEFAULT_SERVER = "https://lulctimeseries.blob.core.windows.net/lulctimeseriesv003/"
DEFAULT_DATA_DIR = os.path.join("/home", "flightgear", "data", "Sentinel-2")
DEBUG = False

def usage(msg, *, exit_status=1, **kwargs):
    help_text = f"""\
ERROR: {msg}

Get ESRI Sentinel-2 10m Landcover (defaults to {DEFAULT_SERVER}), for use by genVPB.py or osgdem.

Usage: {PROGNAME} <min-lat> <min-lon> <max-lat> <max-lon> [--server <server>] [--output-dir <data-dir>] [--year <year>]

  <min-lat> <min-lon> <max-lat> <max-lon> - bounding box of landcover to download
  [server]        - Optional server to retrieve from.  Defaults to {DEFAULT_SERVER}
  [output-dir]    - Optional directory containing landcover GeoTIFFs. Defaults to '{DEFAULT_DATA_DIR}
  [year]          - Optional year to download.  Will download data from [year-1, year].  Defaults to the current year'"""
    
    print(help_text, **kwargs)
    sys.exit(exit_status)

class MGRSError(Exception):
    """Custom exception for MGRS conversion errors"""
    pass

class DownloadError(Exception):
    """Custom exception for download errors"""
    pass

class DiskSpaceError(Exception):
    """Custom exception for insufficient disk space"""
    pass

def check_disk_space(path: Path, required_bytes: int = 1_000_000_000) -> bool:
    """
    Check if there's enough disk space available.
    Default requirement: 1GB
    """
    free_space = shutil.disk_usage(path).free
    return free_space > required_bytes

def get_mgrs_tile(lat: float, lon: float) -> str:
    """
    Get the MGRS tile identifier for given coordinates using the mgrs library.
    
    Args:
        lat: Latitude in decimal degrees (-90 to 90)
        lon: Longitude in decimal degrees (-180 to 180)
        
    Returns:
        str: MGRS tile identifier (e.g., "31TCJ")
        
    Raises:
        MGRSError: If MGRS conversion fails
        ValueError: If coordinates are outside valid ranges
    """
    try:
        # Validate input coordinates
        if not (-90 < lat < 90):  # Exclude exact poles
            raise ValueError(f"Latitude {lat} must be between -90 and 90 degrees (exclusive)")
        if not (-180 <= lon <= 180):
            raise ValueError(f"Longitude {lon} must be between -180 and 180 degrees")
            
        # Handle international date line
        if lon == 180:
            lon = -180
            
        m = mgrs.MGRS()
        mgrs_str = m.toMGRS(lat, lon, MGRSPrecision=5)
        
        # Extract Grid Zone Designator, latitude band, and 100km Grid Square
        return mgrs_str[:3]  # Changed from 3 to 5 to include 100km grid square
        
    except Exception as e:
        raise MGRSError(f"MGRS conversion failed for coordinates ({lat}, {lon}): {str(e)}")


def getTileFilename(
    lat: float, 
    lon: float, 
    year: int = None) -> str :

    # Use current year if none specified
    if year is None:
        year = datetime.now().year
    
    # Validate year
    current_year = datetime.now().year
    if not (2020 <= year <= current_year):
        raise ValueError(f"Year must be between 2020 and {current_year}")
    
    # Get MGRS tile identifier
    mgrs_tile = get_mgrs_tile(lat, lon)
    if DEBUG: print(f"MGRS tile identifier: {mgrs_tile}")

    # Generate the GeoTIFF filename and URL
    start_date = f"{year-1}0101"
    end_date = f"{year}0101"
    return f"{mgrs_tile}_{start_date}-{end_date}.tif"     

def validate_filename(filename: str) -> bool:
    """Validate filename for illegal characters and length"""
    if not filename:
        return False
    
    # Check for illegal characters in different OS
    illegal_chars = '<>:"/\\|?*'
    if any(char in filename for char in illegal_chars):
        return False
    
    # Check length (Windows has 260 char path limit, leave room for path)
    if len(filename) > 240:
        return False
        
    return True

def download_with_retry(url: str, output_path: str, timeout: int = 30):
    """Download with retry logic and timeout"""
    session = requests.Session()
    retry_strategy = Retry(
        total=3,
        backoff_factor=1,
        status_forcelist=[500, 502, 503, 504]
    )
    adapter = HTTPAdapter(max_retries=retry_strategy)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    
    try:
        # Stream download to allow for large files
        print(f"Downloading {url} ... ", end="", flush=True)
        with session.get(url, stream=True, timeout=timeout) as r:
            r.raise_for_status()
            with open(output_path, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)
    except Exception as e:
        # Clean up partial download if it exists
        if os.path.exists(output_path):
            os.remove(output_path)
        raise DownloadError(f"Download failed: {str(e)}")

def downloadSentinel(
    lat: float, 
    lon: float, 
    output_dir: str,
    year: int = None, 
    timeout: int = 30,
    base_url: str = DEFAULT_SERVER
) -> str:
    """
    Generate URL and download Sentinel-2 10m Land Use/Land Cover GeoTIFF.
    
    Args:
        lat: Latitude in decimal degrees (-90 to 90)
        lon: Longitude in decimal degrees (-180 to 180)
        year: Year for the LULC data (defaults to current year)
        output_dir: Directory to save the downloaded file
        timeout: Download timeout in seconds (default 30)
        
    Returns:
        Path to file
        
    Raises:
        ValueError: If coordinates are invalid or year is invalid
        MGRSError: If MGRS conversion fails
        DownloadError: If download fails
        DiskSpaceError: If insufficient disk space
    """
    try:
        filename = getTileFilename(lat=lat, lon=lon, year=year)

        if year is None:
            year = datetime.now().year

        geotiff_url = (
            f"{base_url}"
            f"lc{year-1}/{filename}"
        )
        if DEBUG: print(f"Generated URL: {geotiff_url}")        
        
        # Validate filename
        if not validate_filename(filename):
            raise ValueError(f"Invalid filename: {filename}")

        output_path = os.path.join(output_dir, filename)
        if os.path.isfile(output_path) : return output_path        

        # Check disk space (assuming typical file size of 1GB)
        if not check_disk_space(output_dir):
            raise DiskSpaceError(f"Insufficient disk space in {output_dir}")
        
        # Download the file with retry logic
        if DEBUG: print(f"Downloading to: {output_path}")
        download_with_retry(geotiff_url, str(output_path), timeout=timeout)
        
        # Verify file was downloaded and has size > 0
        if not os.path.isfile(output_path) or os.stat(output_path).st_size == 0:
            raise DownloadError("Download failed or file is empty")
            
        if DEBUG: print("Download completed successfully")
        return output_path
        
    except (ValueError, MGRSError, DiskSpaceError) as e:
        raise e
    except Exception as e:
        raise DownloadError(f"Download failed: {str(e)}")

def getSentinel(
    lat: float, 
    lon: float, 
    output_dir: str,
    year: int = None, 
    timeout: int = 30,
    base_url: str = DEFAULT_SERVER
) -> str:
    """
    Download and process Sentinel-2 10m Land Use/Land Cover GeoTIFF.
    
    Args:
        lat: Latitude in decimal degrees (-90 to 90)
        lon: Longitude in decimal degrees (-180 to 180)
        year: Year for the LULC data (defaults to current year)
        output_dir: Directory to save the downloaded file
        
    Returns:
        Path: Path to the downloaded file
        
    Raises:
        ValueError: If coordinates are invalid or year is invalid
        MGRSError: If MGRS conversion fails
        DownloadError: If download fails
        DiskSpaceError: If insufficient disk space
    """

    # Check if the file exists already, in which case we assume it's good and nothing
    # further is required.
    filename = getTileFilename(lat=lat, lon=lon, year=year)
    output_path = os.path.join(output_dir, filename)
    if os.path.isfile(output_path) : return output_path        

    file_path = downloadSentinel(lat=lat, lon=lon, output_dir=output_dir, year=year, timeout=timeout, base_url=base_url)

    # CRS will be UTM, and needs to be converted to WGS84.
    print(f"Reading raster {file_path} ... ", end="", flush=True)
    dataset = gdal.Open(file_path)
    print("DONE.", flush=True)

    # Check and correct the projection
    projection = dataset.GetProjection()
    if not projection.startswith("GEOGCS[\"WGS 84\"") :
        print("Warping raster to WGS84 ... ", end="", flush=True)
        dataset = gdal.Warp('', dataset, format = 'MEM', dstSRS = 'EPSG:4326')
        print("done.", flush=True)

    width = dataset.RasterXSize
    height = dataset.RasterYSize
    gt = dataset.GetGeoTransform()

    # It is quite likely that the reprojection won't quite align with degree boundaries
    # or that there are cloud areas (landclass 10)
    # so we resize the raster to a degree boundary, and extrapolate to fill the resulting NoData
    lon0 = round(gt[0], 1)
    lat0 = round(gt[3] + width*gt[4] + height*gt[5], 1)
    lon1 = round(gt[0] + width*gt[1] + height*gt[2], 1)
    lat1 = round(gt[3], 1)

    dataset  = gdal.Warp('', dataset, format= 'MEM', outputBounds=(lon0, lat0, lon1, lat1))
    band = dataset.GetRasterBand(1)

    # Grow into the NoData area
    for i in range(3) :
        result = gdal.FillNodata(targetBand = band, maskBand = None, maxSearchDist=1, smoothingIterations=0)
        if (result) : 
            usage("Failed to fill No Data")

    band = None # Flush to the dataset

    # Write it back out.
    if DEBUG : print("Writing " + file_path)
    driver = gdal.GetDriverByName("GTiff")
    outdata = driver.Create(file_path, dataset.RasterXSize, dataset.RasterYSize, 1, gdal.GDT_Byte)
    outdata.SetProjection(dataset.GetProjection())
    outdata.SetGeoTransform(dataset.GetGeoTransform())
    band = dataset.GetRasterBand(1)
    arr = band.ReadAsArray()
    outband = outdata.GetRasterBand(1)
    outband.WriteArray(arr)
    if (band.GetNoDataValue() == None) :
        outband.SetNoDataValue(0)
    else :
        outband.SetNoDataValue(band.GetNoDataValue())
    outdata.FlushCache()
    outdata = None
    band = None

    return file_path

def main():

    if len(sys.argv) < 5:
        usage("")

    lat0, lon0, lat1, lon1 = map(int, sys.argv[1:5])
    output_dir = DEFAULT_DATA_DIR
    year = None

    # Parse optional command line arguments.
    i = 5
    while i < len(sys.argv):
        arg = sys.argv[i]

        if arg == '--server':
            i = i + 1
            server = str(sys.argv[i])

        elif arg == '--output-dir':
            i = i + 1
            output_dir = str(sys.argv[i])

        elif arg == '--year':
            i = i + 1
            year = int(sys.argv[i])

        else:
            usage("Invalid argument: {}".format(arg))

        i = i + 1

    # Basic argument validation
    if lon0 >= lon1:
        usage("<min-lon> must be smaller than <max-lon>")
    if lat0 >= lat1:
        usage("<min-lat> must be smaller than <max-lat>")
    if not os.path.isdir(output_dir):
        usage("<output-dir> (" + output_dir + ") does not exist")

    # Now do the work
    for lon in range(lon0, lon1):
        for lat in range(lat0, lat1):            
            try:
                getSentinel(lon=lon, lat=lat, output_dir=output_dir, year=year)
            except (MGRSError, DownloadError) as e:
                print(f"Failed to get Sentinel-2 tile for {lat}, {lon} - {e}")

if __name__ == "__main__": main()