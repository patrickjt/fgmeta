# QGIS Sentinel-2 reclassification rules table
#
# See https://www.arcgis.com/home/item.html?id=d3da5dd386d140cf93fc9ecbf8da5e31
# for information of the various landclass definitions. 
#
# Note that SENTINEL-2 has very few landclasses.  Some interpretation
# may be required to assign a suitable landclass for an area
#
# To use:  Select r.reclass from the Processing Toolbox
# r.reclass
# 
# Either use this file as-is or copy the text below
# into the "Reclass rules text" section

# Water  (41 Water or 44 Ocean )
# This should almost always be Water (41) be Water, and rely on the OSM land polygon to define the Ocean
1 = 41

# Trees. Note that there is no differentiation of tree type.
# Choose something suitable for the region
2 = 23

# Flooded vegetation.  Could be rice paddies, marsh etc.
4 = 35

# Crops
5 = 21

# Built up areas
7 = 2

# Bare ground
8 = 31

# Snow/Ice
9 = 34

# Clouds.  Mapping to rangeland for convenience.
10 = 26

# Rangeland
11 = 26

# Everything else is mapped to water
* = 44
