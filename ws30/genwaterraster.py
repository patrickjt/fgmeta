#!/usr/bin/python3

# gencoastlineraster.py - create raster of coastline from OpenStreetMap
# Copyright (C) 2021-2024  Stuart Buchanan
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# You will need Python Overpass API and pixie library - "pip install overpy pixie"

from typing import Tuple
from math import floor

import os
import sys
import pixie

import overpy
import requests
import json
import subprocess
import math
import zipfile

# Lowest LoD is 1/32 degree wide and high.  At the equator, that equates to 4.90km diagonal.  At 45 degrees 4.25km, and at 60 degrees 3.89 degrees
# Unfortunately it's also not square.
X_SIZE = 1024
Y_SIZE = 1024
DELTA = float(1/32.0)
METERS_PX = float(4900.0 / math.sqrt(X_SIZE * X_SIZE + Y_SIZE * Y_SIZE)); # 3.3 m/px
OVERPASS_URL = "https://overpass.kumi.systems/api/interpreter"

nodes = {}
curr_coast_count = 0
curr_water_count = 0
curr_poly_count = 0
curr_ring_error_count = 0
curr_api_error_count = 0
coast_count = 0
water_count = 0
poly_count = 0
ring_error_count = 0
api_error_count = 0
cache_dir = None
DEBUG = False

# Configure compression of the final data.  Configuration provided for backwards compatibility
COMPRESS = True

PROGNAME = os.path.basename(sys.argv[0])

def usage(msg, *, exit_status=1, **kwargs):
    help_text = f"""\
ERROR: {msg}

Generation of coastline and water raster files for WS3.0.

Usage: {PROGNAME} <min-lat> <min-lon> <max-lat> <max-lon> <scenery-dir> [cache-dir]

  <min-lat> <min-lon> <max-lat> <max-lon - bounding box of water to generated
  <scenery_dir>  Scenery directory to write to
  [Cache]        Optional directory to cache OpenStreetMap data to reduce API requests
'"""
    print(help_text, **kwargs)
    sys.exit(exit_status)

def format_lon(lon):
    """Format longitude as e/w."""
    if lon < 0.:
        return "w%03d" % int(0. - lon)
    else:
        return "e%03d" % int(lon)


def format_lat(lat):
    """Format latitude as n/s."""
    if lat < 0.:
        return "s%02d" % int(0. - lat)
    else:
        return "n%02d" % int(lat)

def dir_10x10(lat_lon: Tuple[float, float]) -> str:
    """Generate the 10x10 degere directory name for a location."""
    (lat, lon) = lat_lon
    lat_chunk = floor(lat/10.0) * 10
    lon_chunk = floor(lon/10.0) * 10
    return format_lon(lon_chunk) + format_lat(lat_chunk)

def dir_1x1(lat_lon: Tuple[float, float]) -> str:
    """Generate the 1x1 directory name for a location."""
    (lat, lon) = lat_lon
    lat_floor = floor(lat)
    lon_floor = floor(lon)
    return format_lon(lon_floor) + format_lat(lat_floor)

def dir_1x1_root(lat_lon: Tuple[float, float]) -> str:
    """Generate the root directory name for a location."""
    (lat, lon) = lat_lon
    lat_floor = floor(lat)
    lon_floor = floor(lon)
    return "ws_" + format_lon(lon_floor) + format_lat(lat_floor) + "_root_L0_X0_Y0"

def directory_name(lat_lon: Tuple[float, float]) -> str:
    """Generate the directory name for a location."""
    return os.path.join(dir_10x10(lat_lon), dir_1x1(lat_lon), dir_1x1_root(lat_lon))

def raster_name(lat_lon: Tuple[float, float], x_y: Tuple[int, int]) -> str:
    """Generate the filename name for a coastline raster."""
    (lat, lon) = lat_lon
    (x, y) = x_y
    lat_floor = floor(lat)
    lon_floor = floor(lon)
    return 'ws_{}{}_L6_X{}_Y{}_coastline.png'.format(format_lon(lon_floor), format_lat(lat_floor), y, x)

def getCart(lat_lon: Tuple[float, float], min_lat_lon: Tuple[float, float]):
    """Get the Cartesian coordinate in a texture for a give lat and lon"""
    (lat, lon) = lat_lon
    (min_lat, min_lon) = min_lat_lon
    x = float((lon - min_lon) / DELTA * X_SIZE)

    # OpenGL coordinates are Y-up rather than Y-down
    y = float((lat - min_lat) / DELTA * Y_SIZE)
    return (x, y)

def osWalkErrorHandling(exception):
    raise exception             # don't silence errors during os.walk()


def compressToZip(destfile, base_dir):
    """Recursively archive files from a directory to a .zip file.

    Positional arguments:

    destfile -- path to the destination zip file
    base_dir -- compress files recursively found there

    If already existing, destfile is truncated. Each file found under
    the directory tree starting at base_dir is stored at the root of the
    archive, as if using zip(1) with the -j option. This can of course
    lead to several archive members having the same name!
    """
    with zipfile.ZipFile(destfile, 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as compressed_file:
        for dirpath, dirs, files in os.walk(base_dir,
                                            onerror=osWalkErrorHandling):
            for f in files:
                compressed_file.write(os.path.join(dirpath, f),
                                      os.path.basename(f))


def createRaster(start_lat, start_lon, result, scenery_prefix: str):
    """Create a set of rasters for a 1x1 degree tile based on some Overpass data"""
    global curr_coast_count, curr_water_count, curr_poly_count, curr_ring_error_count, curr_api_error_count
    lat_lon = (start_lat, start_lon)

    if COMPRESS:
        # Firstly extract any existing compressed archive so we can
        # update the contents, overwriting any existing files.  In particular
        # this allows genVPB.py and genwaterraster.py to be run in any order
        zipfile_path = os.path.join(scenery_prefix, dir_10x10(lat_lon), dir_1x1(lat_lon) + ".zip")
        base_dir = os.path.join(scenery_prefix, directory_name(lat_lon))

        if os.path.exists(zipfile_path):
            with zipfile.ZipFile(zipfile_path, 'r') as compressed_file:
                compressed_file.extractall(base_dir)


    for i in range(0, 32):
        for j in range(0, 32):
            min_lat = float(start_lat + i * DELTA)
            min_lon = float(start_lon + j * DELTA)
            ways = []
            relations = []

            for way in result.ways:
                if way.tags.get("tunnel") == None:
                    nodes = way.get_nodes(resolve_missing=True)
                    for pt in nodes:
                        lat = float(pt.lat)
                        lon = float(pt.lon)
                        if (min_lon <= lon <= (min_lon + DELTA)) and (min_lat <= lat <= (min_lat + DELTA)):
                            #print("  Match: ({},{})".format(lat,lon))
                            ways.append(way)
                            break

            for relation in result.relations:
                for w in relation.members:
                    try:
                        way = result.get_way(w.ref, resolve_missing=True)
                        nodes = way.get_nodes(resolve_missing=True)
                        for pt in nodes:
                            lat = float(pt.lat)
                            lon = float(pt.lon)
                            if (min_lon <= lon <= (min_lon + DELTA)) and (min_lat <= lat <= (min_lat + DELTA)):
                                #print("  Match: ({},{})".format(lat,lon))
                                relations.append(relation)
                                break
                        if relations.count(relation) > 0: break
                    except: 
                        curr_api_error_count += 1

            if (len(ways) > 0) or (len(relations) > 0):
                # We have something to write for this L6 node.
                image = pixie.Image(X_SIZE, Y_SIZE)
                image.fill(pixie.Color(0, 0, 0, 1))

                if (DEBUG):
                    font = pixie.read_font("/usr/share/fonts/truetype/freefont/FreeSans.ttf")
                    font.size = 20
                    font.paint.color = pixie.Color(0,0,1,1)

                    text = raster_name((lat, lon), (i, j))

                    image.fill_text(
                        font,
                        text,
                        bounds = pixie.Vector2(1024, 180),
                        transform = pixie.translate(10, 10)
                    )

                # We have different paints for generic water features
                # and coastlines.  This is because we allow generic water
                # features such as rivers to be water even if on a slope,
                # while the sea is always flat.  The green channel is used
                # to control this by modifying the apparent steepness of
                # the fragment
                waterPaint = pixie.Paint(pixie.SOLID_PAINT)
                waterPaint.color = pixie.Color(0, 1, 1, 1)

                coastlinePaint = pixie.Paint(pixie.SOLID_PAINT)
                coastlinePaint.color = pixie.Color(0, 0, 1, 1)

                min_lat_lon=(min_lat, min_lon)
                waterPath = pixie.Path()
                coastlinePath = pixie.Path()

                for r in relations:
                    # This is a multipolygon, so fill it taking into account the outer vs inner by using
                    # winding rules.
                    curr_poly_count += 1
                    error = False

                    # This is complicated by the need to find closed rings, which can consist of multiple unclosed ways.
                    rways = []
                    for m in r.members:
                        try:
                            w = result.get_way(m.ref, resolve_missing=True)
                            rways.append(w)
                        except: 
                            curr_api_error_count += 1

                    polyPath = pixie.Path()
                    while len(rways) > 0:
                        # Get a way.  We will now try to find the set of ways that includes
                        # this way and is a closed ring. 
                        rway = rways.pop()
                        rnodes = rway.nodes
                        role = rway.tags.get("role")

                        # This is the start of the ring.  
                        start_node = rnodes[0]
                        last_node = rnodes[-1]
                        outer_error = False

                        while ((last_node != start_node) and (outer_error == False)):
                            # We haven't yet found a closed ring.  So try to find the next way that links to the
                            # end of this one
                            found = False
                            for w2 in rways:
                                if (w2.nodes[0] == last_node):
                                    # We found an way that connects to our existing way, so add it to the end
                                    # and start looking for new last node.
                                    rnodes.extend(w2.nodes[1:])
                                    last_node = rnodes[-1]
                                    rways.remove(w2)
                                    found = True
                                    break

                                if (w2.nodes[-1] == last_node):
                                    # In theory, OSM data should link tail -> head of each way.  However we
                                    # have found data where this is not the case and the data links tail->tail.
                                    # So we need to reverse the new way when appending it.
                                    sub = w2.nodes[:-1]
                                    sub.reverse()
                                    rnodes.extend(sub)
                                    last_node = rnodes[-1]
                                    rways.remove(w2)
                                    found = True
                                    break

                            if not found:
                                # If we get to this point in the while loop we didn't find a matching way that 
                                # connects to the current last node.  I.e. we cannot close this ring
                                if (role == "outer"):
                                    # An error in the outer part of a multipolygon is a BIG problem
                                    # because it will invert the resulting polygon due to the EVEN_ODD
                                    # winding rule.  However, an error in an inner part of the 
                                    # multipolygon just means that some island will be displayed as water.
                                    outer_error = True
                                break

                        #  At this point we've either closed the ring, or hit an error
                        if (last_node == start_node):
                            # We closed the ring, so add it to the path
                            (x,y) = getCart(lat_lon=(rnodes[0].lat, rnodes[0].lon), min_lat_lon=min_lat_lon)
                            polyPath.move_to(x,y)
                            for pt in rnodes[1:]:
                                # At the ranges we are talking about we will just assume linear mapping
                                (x,y) = getCart(lat_lon=(pt.lat, pt.lon), min_lat_lon=min_lat_lon)
                                polyPath.line_to(x,y)
                        else:
                            # We failed to close the ring.
                            curr_ring_error_count += 1

                    # At this point we have assigned each way to a ring (which may be closed or not)

                    if outer_error == False:
                        # We didn't have an error in the outer ring so write the path to the raster
                        image.fill_path(polyPath, waterPaint, winding_rule=pixie.EVEN_ODD)

                for w in ways:
                    if (w.tags.get("natural") == "water"):
                        # This is a simple polygon, so fill it
                        if DEBUG and w.tags.get("name"):
                            print("Writing water feature {}".format(w.tags.get("name")))
                        polyPath = pixie.Path()
                        (x,y) = getCart(lat_lon=(w.nodes[0].lat, w.nodes[0].lon), min_lat_lon=min_lat_lon)
                        polyPath.move_to(x,y)
                        for pt in w.nodes[1:]:
                            # At the ranges we are talking about we will just assume linear mapping
                            (x,y) = getCart(lat_lon=(pt.lat, pt.lon), min_lat_lon=min_lat_lon)
                            polyPath.line_to(x,y)

                        image.fill_path(polyPath, waterPaint)
                        curr_poly_count += 1
                    elif (w.tags.get("natural") == "coastline"):
                        # This is a line, so just add it to the main coastline path
                        (x,y) = getCart(lat_lon=(w.nodes[0].lat, w.nodes[0].lon), min_lat_lon=min_lat_lon)
                        coastlinePath.move_to(x,y)
                        curr_coast_count += 1
                        for pt in w.nodes[1:]:
                            # At the ranges we are talking about we will just assume linear mapping
                            (x,y) = getCart(lat_lon=(pt.lat, pt.lon), min_lat_lon=min_lat_lon)
                            coastlinePath.line_to(x,y)
                    else:
                        # This is a line, so just add it to the main path
                        (x,y) = getCart(lat_lon=(w.nodes[0].lat, w.nodes[0].lon), min_lat_lon=min_lat_lon)
                        waterPath.move_to(x,y)
                        curr_water_count += 1
                        for pt in w.nodes[1:]:
                            # At the ranges we are talking about we will just assume linear mapping
                            (x,y) = getCart(lat_lon=(pt.lat, pt.lon), min_lat_lon=min_lat_lon)
                            waterPath.line_to(x,y)

                # Calculate appropriate widths for the different paths.
                water_width = round(3 / METERS_PX)
                coastline_width = round(18 / METERS_PX)

                image.stroke_path(path=waterPath, paint=waterPaint, transform=None, stroke_width=water_width, line_cap=pixie.ROUND_CAP, line_join=pixie.ROUND_JOIN)
                image.stroke_path(path=coastlinePath, paint=coastlinePaint, transform=None, stroke_width=coastline_width, line_cap=pixie.ROUND_CAP, line_join=pixie.ROUND_JOIN)
                raster_file = os.path.join(scenery_prefix, directory_name(lat_lon), raster_name(lat_lon, (i, j)))
                os.makedirs(os.path.join(scenery_prefix, directory_name(lat_lon)), exist_ok=True)
                image.write_file(raster_file)

                # Now run the convert program to minimize size by converting to png8.  E.g. Indexed PNG.
                # For some reason the PNG writer writes out transparency as 255,255,255,255.  So when
                # we remove the alpha layer we also need to map that back to black.
                args = [
                    "convert",
                    raster_file,
                    "-alpha", "off",
                    "-fill", "srgb(0,0,0)",
                    "-opaque", "srgb(255,255,255)",
                    "png8:{}".format(raster_file)
                ]

                convert = subprocess.run(args, capture_output=True, check=True, text=True)
                if DEBUG: print("stdout: '{}'".format(convert.stdout))
                if DEBUG: print("stderr: '{}'".format(convert.stderr))

    if COMPRESS and os.path.isdir(os.path.join(scenery_prefix, directory_name(lat_lon))):
        # Compress final output by creating a zip file. Note that the directory
        # structure is removed so as to work around a bug in OSG's relative
        # path loading of zip files.
        zipfile_path = os.path.join(scenery_prefix, dir_10x10(lat_lon), dir_1x1(lat_lon) + ".zip")
        base_dir = os.path.join(scenery_prefix, directory_name(lat_lon))
        compressToZip(zipfile_path, base_dir)

        # Now remove the output directory carefully.  There should only be
        # files within the tile directory, after which we should be able to 
        # delete the tiledirectory, the originally generated osgb file, and
        # the output directory.
        for dirpath, dirs, files in os.walk(base_dir, onerror=osWalkErrorHandling):
            for f in files:
                os.unlink(os.path.join(dirpath, f))

        os.rmdir(os.path.join(base_dir))
        os.rmdir(os.path.join(os.path.join(scenery_prefix, dir_10x10(lat_lon), dir_1x1(lat_lon))))

def genWaterRaster(lat : int, lon : int, scenery_dir : str, cache_dir : str ) :
    global coast_count, water_count, poly_count, ring_error_count, api_error_count
    global curr_coast_count, curr_water_count, curr_poly_count, curr_ring_error_count, curr_api_error_count

    osm_bbox = ",".join([str(lat), str(lon), str(lat+1), str(lon+1)])

    curr_coast_count = 0
    curr_water_count = 0
    curr_poly_count = 0
    curr_api_error_count = 0
    curr_ring_error_count = 0
    curr_api_error_count = 0
    result = None

    if cache_dir:
        cache_file = os.path.join(cache_dir, "water_" + format_lon(lon) + format_lat(lat) + ".osm")
        if (os.path.isfile(cache_file)):
            with open(cache_file, 'r', encoding="utf-8") as f:
                    js = json.load(f)
                    result = overpy.Result.from_json(js, api=overpy.Overpass(url=OVERPASS_URL))
                    #print("Read cached osm file: " + cache_file)
                    createRaster(lat, lon, result, scenery_dir)

    if result == None:
        # If we don't have a result, retrieve data from OSM Overpass API
        water_query = "[bbox:" + osm_bbox + "][out:json];(relation[\"natural\"=\"water\"];way[\"natural\"=\"water\"];way[\"natural\"=\"coastline\"];way[\"waterway\"=\"river\"]; way[\"waterway\"=\"canal\"];);(._;>;);out;"

        #water_query = "[out:json];(relation[\"natural\"=\"water\"](" + osm_bbox + ");way[\"natural\"=\"water\"](" + osm_bbox + ");way[\"natural\"=\"coastline\"](" + osm_bbox + ");way[\"waterway\"=\"river\"](" + osm_bbox + "); way[\"waterway\"=\"canal\"](" + osm_bbox + ");); out qt; >; out skel qt;"
        #print(water_query);

        response = requests.post(OVERPASS_URL, data=water_query)
        js = response.json()
        result = overpy.Result.from_json(js)
        createRaster(lat, lon, result, scenery_dir)

        #result = api.query(water_query)
        # Save off the results to the cache only after processessing so that any resolution of missing nodes has taken place.
        if cache_dir != None:
            with open(cache_file, 'w', encoding="utf-8") as f:
                json.dump(js, f, separators=(',', ':') )
                f.close()
            
    if DEBUG : print("{},{}: {} coastlines, {} waterways, {} polygons of water, {} api errors, {} ring errors".format(lat, lon, curr_coast_count, curr_water_count, curr_poly_count, curr_api_error_count, curr_ring_error_count))
    
    coast_count = coast_count + curr_coast_count
    water_count = water_count + curr_water_count
    poly_count = poly_count + curr_poly_count
    ring_error_count = ring_error_count + curr_ring_error_count
    api_error_count = api_error_count + curr_api_error_count


def main():

    if len(sys.argv) < 6:
        usage("")

    lat1 = sys.argv[1]
    lon1 = sys.argv[2]
    lat2 = sys.argv[3]
    lon2 = sys.argv[4]
    scenery_prefix = sys.argv[5]

    if (len(sys.argv) > 6):
        cache_dir = sys.argv[6]
        os.makedirs(cache_dir, exist_ok=True)
        if not os.path.isdir(cache_dir):
            usage("Unable to create {} directory for cache.".format(cache_dir))

    if (not(scenery_prefix.endswith("/vpb")) and not(scenery_prefix.endswith("/vpb/"))):
        usage("Scenery directory incorrect - should end with a /vpb")

    os.makedirs(scenery_prefix, exist_ok=True)
    if not os.path.isdir(scenery_prefix):
        usage("Unable to create {} directory for scenery.".format(scenery_prefix))

    for lat in range(int(lat1), int(lat2)):
        for lon in range(int(lon1), int(lon2)):
            genWaterRaster(lat=lat, lon=lon, scenery_dir=scenery_prefix, cache_dir=cache_dir)

    print("Wrote total of {} coastlines, {} waterways, {} polygons of water, {} api errors, {} ring errors.".format(coast_count, water_count, poly_count, api_error_count, ring_error_count))

if __name__ == "__main__": main()
