#!/usr/bin/python3

# genroads.py - create appropriate ROAD_LIST STG files for ws30 from openstreetmap
# Copyright (C) 2021  Stuart Buchanan
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Simple generate of line feature - roads, railways, rivers
# You will need Python Overpass API - "pip install overpy"

import xml.etree.ElementTree as etree
import os
import shutil
import re
import sys
import gzip
from math import floor

import calc_tile
import overpy
import requests
import json

# Configure compression of the final data.  Configuration provided for backwards compatibility
COMPRESS = True

PROGNAME = os.path.basename(sys.argv[0])

nodes = {}
cache_dir = None
road_count = 0
rail_count = 0

if (len(sys.argv) < 6):
    print("Simple generation of LINEAR_FEATURE_LIST files")
    print("")
    print("Usage: " + PROGNAME + " <lat1> <lon1> <lat2> <lon2> <scenery_dir> [Cache]")
    print("  <lat1> <lon1>  \tBottom left lat/lon of bounding box")
    print("  <lat2> <lon2>  \tTop right lat/lon of bounding box")
    print("  <scenery_dir>  \tScenery directory to write to")
    print("  [Cache]        \tOptional directory to cache OpenStreetMap data to reduce API requests")
    sys.exit(1)

lat1 = sys.argv[1]
lon1 = sys.argv[2]
lat2 = sys.argv[3]
lon2 = sys.argv[4]
scenery_prefix = sys.argv[5]

if (len(sys.argv) > 6):
    cache_dir = sys.argv[6]
    os.makedirs(cache_dir, exist_ok=True)
    if not os.path.isdir(cache_dir):
        print("Unable to create {} directory for cache.".format(cache_dir))
        sys.exit(1)

if (not(scenery_prefix.endswith("/Terrain")) and not(scenery_prefix.endswith("/Terrain/"))):
    print("Scenery directory incorrect - should end with a /Terrain")
    sys.exit(1)

os.makedirs(scenery_prefix, exist_ok=True)
if not os.path.isdir(scenery_prefix):
    print("Unable to create {} directory for scenery.".format(scenery_prefix))
    sys.exit(1)

def format_lon(lon):
    """Format longitude as e/w."""
    if lon < 0.:
        return "w%03d" % int(0. - lon)
    else:
        return "e%03d" % int(lon)


def format_lat(lat):
    """Format latitude as n/s."""
    if lat < 0.:
        return "s%02d" % int(0. - lat)
    else:
        return "n%02d" % int(lat)

def feature_file(lat, lon, type):
    index = calc_tile.calc_tile_index((lon,lat))
    return str(index) + "_" + type + ".txt"

def add_to_stg(lat, lon, type):
    index = calc_tile.calc_tile_index((lon, lat))
    stg = os.path.join(scenery_prefix, calc_tile.directory_name((lon, lat)), str(index) + ".stg")
    #print("Writing " + stg)
    with open(stg, 'a', encoding="utf-8") as f:
        f.write("LINE_FEATURE_LIST " + feature_file(lat, lon, type) + " " + type + "\n")

def write_feature(lon, lat, road, type, width, lit):
    index = calc_tile.calc_tile_index((lon,lat))
    dirname = os.path.join(scenery_prefix, calc_tile.directory_name((lon, lat)))
    os.makedirs(dirname, exist_ok=True)
    txt = os.path.join(scenery_prefix, calc_tile.directory_name((lon, lat)), feature_file(lat, lon, type))
    #print("Writing " + txt)

    if COMPRESS:
        with gzip.open(txt + ".gz", 'at') as f:
            f.write(str(width) + " " + str(lit) + " 1 1 1 1") # Width plus currently unused generic attributes.
            for pt in road:
                f.write(" "  + str(pt.lon) + " " + str(pt.lat))
            f.write("\n")
    else:
        with open(txt, 'a', encoding="utf-8") as f:
            f.write(str(width) + " " + str(lit) + " 1 1 1 1") # Width plus currently unused generic attributes.
            for pt in road:
                f.write(" "  + str(pt.lon) + " " + str(pt.lat))
            f.write("\n")

    stg = os.path.join(scenery_prefix, calc_tile.directory_name((lon, lat)), str(index) + ".stg")
    if not os.path.isfile(stg):
        # No STG - generate
        add_to_stg(lat, lon, type)
    else:
        road_exists = False
        with open(stg, 'r', encoding="utf-8") as f:
            for line in f:
                if line.startswith("LINE_FEATURE_LIST " + feature_file(lat, lon, type)):
                    road_exists = True
        if not road_exists:
            add_to_stg(lat, lon, type)


def parse_way(way):
    global curr_road_count, curr_rail_count, lat1, lon1, lat2, lon2
    pts = []    
    width = 6.0
    lit = 0

    highway = way.tags.get("highway")
    railway = way.tags.get("railway")
    feature_type = "None"

    if way.tags.get("tunnel") == None or way.tags.get("tunnel") != "yes":

        if (highway=="motorway_junction") or (highway=="motorway") or (highway=="motorway_link"):
            width = 15.0
            feature_type = "ws30Freeway"
            curr_road_count += 1

        if (highway=="primary")  or (highway=="trunk") or (highway=="trunk_link") or (highway=="primary_link"):
            width = 12.0
            feature_type = "ws30Freeway"
            curr_road_count += 1

        if (highway=="secondary") or (highway=="secondary_link"):
            width = 12.0
            feature_type = "ws30Road"
            curr_road_count += 1

        if (highway=="unclassified") or (highway=="tertiary") or (highway=="tertiary_link") or (highway=="service") or (highway=="residential"):
            width = 6.0
            feature_type = "ws30Road"
            curr_road_count += 1

        if (railway=="rail") or (railway=="preserved") or (railway=="disused"):
            width = 4.2  # Standard guage ~ 1.4m, with twice the space either side
            feature_type = "ws30Railway"
            curr_rail_count += 1

        # Use the width if defined and parseable
        if (way.tags.get("width") is not None):
            width_str = way.tags.get("width")
            try:
                if (' m' in width_str):
                    width = float(width_str[0:width_str.find(" m")])
                if (' ft' in width_str):
                    width = 0.3 * float(width_str[0:width_str.find(" ft")])
            except ValueError:
                print("Unable to parse width " + width_str)

        # Use the lit tag if defined and parseable
        if (way.tags.get("lit") is not None):
            lit_str = way.tags.get("lit")
            if ((lit_str == "no") or (lit == "disused")):
                # Specific tags for unlit ways
                lit = 0
            else:
                # Everything else indicates some form of lighting
                lit = 1

        if (feature_type != "None"):
            # It's a road or railway.  Add it to appropriate tile entries.
            tileids = set()

            for pt in way.nodes:
                lon = float(pt.lon)
                lat = float(pt.lat)
                idx = calc_tile.calc_tile_index([lon, lat])
                if ((float(lon1) <= lon <= float(lon2)) and (float(lat1) <= lat <= float(lat2)) and (idx not in tileids)):
                    # Write the feature to a bucket provided it's within the lat/lon bounds and if we've not already written it there
                    write_feature(lon, lat, way.nodes, feature_type, width, lit)
                    tileids.add(idx)

def writeOSM(result):
        for child in result.ways:
            parse_way(child)

for lat in range(int(lat1), int(lat2)):
    for lon in range(int(lon1), int(lon2)):
        result = None
        curr_road_count = 0
        curr_rail_count = 0
        osm_bbox = ",".join([str(lat), str(lon), str(lat+1), str(lon+1)])


        if cache_dir:
            cache_file = os.path.join(cache_dir, "roads_" + format_lon(lon) + format_lat(lat) + ".osm")
            if (os.path.isfile(cache_file)):
                with open(cache_file, 'r', encoding="utf-8") as f:
                        js = json.load(f)
                        result = overpy.Result.from_json(js)
                        #print("Read cached osm file: " + cache_file)

        if result == None:
            overpass_url = "https://overpass.kumi.systems/api/interpreter"
            query = "[out:json];("
            railway_types = ["rail", "preserved", "disused", "subway", "narrow_gauge"]
            road_types = ["motorway", "trunk", "primary", "secondary", "tertiary", "unclassified", "residential", "motorway_link", "trunk_link", "primary_link", "secondary_link", "tertiary_link"]

            for r in railway_types:
                query = query + "way[\"railway\"=\"" + r + "\"](" + osm_bbox + ");"

            for r in road_types:
                query = query + "way[\"highway\"=\"" + r + "\"](" + osm_bbox + ");"
            query = query + ");(._;>;);out;"

            response = requests.post(overpass_url, data=query)
            js = response.json()
            result = overpy.Result.from_json(js)

            #result = api.query(coast_query)
            if cache_dir is not None:
                with open(cache_file, 'w', encoding="utf-8") as f:
                    json.dump(js, f, separators=(',', ':') )
                    f.close()

        writeOSM(result)

        print(str(lat) + "," + str(lon) + ": " + str(curr_road_count) + " roads " + str(curr_rail_count) + " railways")
        road_count += curr_road_count
        rail_count += curr_rail_count

print("Wrote total of " + str(road_count) + " roads " + str(rail_count) + " railways")
