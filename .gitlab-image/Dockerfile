# SPDX-FileCopyrightText: James Turner <james@flightgear.org>
#
# SPDX-License-Identifier: GPL-2.0

# rebuild commands 
#   docker build -t registry.gitlab.com/flightgear/flightgear .gitlab-image/
#   docker push registry.gitlab.com/flightgear/flightgear

FROM ubuntu:jammy

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install --yes \
        build-essential \
        libgl-dev \
        file \
        ninja-build \
        libssl-dev \
        libfuse2 \
        fuse \
        libcurl4-openssl-dev \
        qt6-base-dev \
        qt6-declarative-dev \
        qt6-tools-dev \
        qt6-tools-dev-tools \
        qt6-l10n-tools \ 
        libqt6svg6-dev \
        qml6-module-qtquick-controls \
        qml6-module-qtquick \
    	qml6-module-qtquick-templates \
        qml6-module-qtqml-workerscript \
        qmake6 \
        libpng-dev \
        libopenal-dev \
        libexpat1-dev \
        libboost-dev \
        liblzma-dev \
        libudev-dev \
        patchelf \
        libfreetype-dev \
        wget \
        ca-certificates gpg \
        libopenxr-dev \
        libevent-dev \
        libdbus-1-dev

# recent Cmake : can be removed if we switch to at least Noble instead of jammy
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
RUN echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ jammy main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null
RUN apt-get update \
    && rm /usr/share/keyrings/kitware-archive-keyring.gpg \
    && apt-get install --yes kitware-archive-keyring cmake

# recent C-ares; Noble has 1.27 which might be sufficient but not 100% sure
ADD https://github.com/c-ares/c-ares.git#v1.34.3 c-ares
RUN cmake -B build-ares -S c-ares -G Ninja -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr \
    && cmake --build build-ares \
    && cmake --install build-ares \
    && rm -rf build-ares
