(Help us help you! The more detailed information you provide in a bug report, the easier it makes our job to fix the bug.
Bug reports that lack critical information are much less likely to lead to a successful resolution.)
## Summary

(Please give a concise summary of the bug you encountered here)

## Steps to reproduce

(Please describe step-by-step, and in detail, how to reproduce your issue. This is very important.)

## What is the current bug behavior?

(Please describe what is actually happening, in as much detail as you can.)

## What is the expected correct behavior?

(Please describe what **should** be happening. Please be as descriptive to the best of your ability, 
and avoid vague statements like "It should work". _What_ should work?)

## Relevant logs and/or screenshots

(Paste any relevant logs - use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.

You should enable logging to help us troubleshoot your issue. At a minimum, add `--log-level=info` to your
command-line arguments, or add it to your settings in the launcher under **Settings > Extra Arguments**.

On Windows, you can find your log file under `%APPDATA%/FlightGear`. On Linux, it will typically be under `~/.fgfs/fgfs.log`. 
On macOS, it can be found under `$HOME/Library/Application Support/FlightGear/fgfs.log`.)

## System and version information 

(This step is critical. Please provide information about your system such as OS version, GPU vendor, etc.)

(If you are building FlightGear from source explicitly provide the commit hashes. If you are using a nightly build,
please specify _which_ nightly version you are using.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~type::bug
