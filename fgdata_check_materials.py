#!/usr/bin/env python3

# prior to running this script, execute the following:
# grep -R MATERIAL --include=*.ac $FG_ROOT > materials.txt

with open("materials.txt", "r") as f:
    for line in f:
        try:
            if int((line.strip()[line.index("MATERIAL"):]).split()[19]) > 128:
                print(line.strip())
        except:
            print("PARSE ERROR:", line.strip())

